import React from "react";
import "./App.css";
import Landing from "./pages/Landing/LandingPage";
import PokePage from "./pages/Pokepage/PokePage";
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import ExamApi from "./pages/ExamApi/ExamApi";
import ExamDetail from "./pages/ExamDetail";

function App() {
  return (
    <Router>
      <Switch>
        <Route path="/about">
          <div>about</div>
        </Route>
        <Route path="/contact">
          <div>contact</div>
        </Route>
        <Route path="/ExamApi/:id">
          <ExamDetail/>
        </Route>
        <Route path="/ExamApi">
          <ExamApi/>
        </Route>
        <Route path="/">
          <Landing />
        </Route>
      </Switch>
    </Router>
  );
}

export default App;

import React from 'react';
import '@testing-library/jest-dom'
import { shallow } from "enzyme";
import FloatingButton from "./index";

describe('Test FloatingButton', ()=>{
    test('<FloatingButton /> good render' ,()=>{
        const item = shallow(<FloatingButton/>);
        expect(item).toMatchSnapshot()
    })
});
import React from "react";
import { FloatingButtonStyled, ContainerButtonFloating } from "./FloatingButton.styled";

const FloatingButton = (props) => {
  return <ContainerButtonFloating onClick={props.action}>
      <FloatingButtonStyled> 
          {props.children}
      </FloatingButtonStyled>
    </ContainerButtonFloating>;
};

export default FloatingButton;

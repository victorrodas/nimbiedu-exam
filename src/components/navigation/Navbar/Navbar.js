import React from "react";
import { Navbar, Container, Nav } from "react-bootstrap";
import { menuNavbar } from "../../../utils/constants/menu.cons";
import { NavBarStyled } from "./Navbar.styled";
import { LinkContainer } from "react-router-bootstrap";
const NavBarLanding = (props) => {
  return (
    <div>
      <NavBarStyled bg="light" expand="lg">
        <Container>
          <Navbar.Brand href="#home">React Test</Navbar.Brand>
          <Navbar.Toggle aria-controls="basic-navbar-nav" />
          <Navbar.Collapse id="basic-navbar-nav">
            <Nav className="me-auto"></Nav>
            <Nav>
              {menuNavbar.map((item, key) => {
                return (
                  <LinkContainer to={item["url"]}>
                    <Nav.Link key={`menu-${key}`}> {item["title"]}</Nav.Link>
                  </LinkContainer>
                );
              })}
            </Nav>
          </Navbar.Collapse>
        </Container>
      </NavBarStyled>
    </div>
  );
};

export default NavBarLanding;

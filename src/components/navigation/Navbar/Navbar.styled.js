import styled from "styled-components";
import { Navbar } from "react-bootstrap";
export const NavBarStyled = styled(Navbar)`
    box-shadow: 0 20px 40px rgb(0 0 0 / 10%);
`;
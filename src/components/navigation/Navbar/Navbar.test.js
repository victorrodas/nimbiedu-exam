import React from 'react';
import '@testing-library/jest-dom'
import { shallow } from "enzyme";
import NavBarLanding from "./Navbar";

describe('Test NavbarLanding', ()=>{
    test('NavBarLanding render' ,()=>{
        const item = shallow(<NavBarLanding />);
        expect(item).toMatchSnapshot()
    })
});
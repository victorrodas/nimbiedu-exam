import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { fetchExamApi } from "./ExamApiSlice";
import { Card } from "react-bootstrap";
import styled from "styled-components";
import { LinkContainer } from "react-router-bootstrap";

const ExamApi = (props) => {
  const messages = useSelector((state) => state.examApiSlice.messages);
  const status = useSelector((state) => state.examApiSlice.status);
  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(fetchExamApi());
  }, [dispatch]);

  return (
    <div>
      {status === "loading" && <div>Cargando.....</div>}
      {status !== "loading" && (
        <div>
          {messages.map((item) => {
            return (
              <LinkContainer to={`/ExamApi/${item["id"]}`}>
                <CardStyled>
                  <div key={`item-${item["id"]}`}>{item["title"]}</div>
                </CardStyled>
              </LinkContainer>
            );
          })}
        </div>
      )}
    </div>
  );
};

export default ExamApi;

const CardStyled = styled(Card)`
  margin: 10px;
  padding: 10px;
`;

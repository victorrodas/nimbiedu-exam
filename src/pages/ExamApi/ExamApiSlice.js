import { createSlice, createAsyncThunk } from "@reduxjs/toolkit";
import axios from "axios";


export const fetchExamApi = createAsyncThunk(
    "team/fetchExamApi",
    async (thunkAPI) => {
      const response = await axios
        .get(`https://jsonplaceholder.typicode.com/posts`)
        .then((response) => response.data)
        .catch((error) => error);
      return response;
    }
  );
  
  export const examApiSlice = createSlice({
    name: "examApliSlice",
    initialState: {
      messages: [],
      status: "loading",
    },
    reducers: {},
    extraReducers: {
      [fetchExamApi.pending.type]: (state, action) => {
          state.messages = [];
          state.status= "loading";
      },
      [fetchExamApi.fulfilled.type]: (state, action) => {
          state.messages = action.payload;
          state.status= "idle";
      },
      [fetchExamApi.rejected.type]: (state, action) => {
        state.messages = []
        state.status= "idle";
      },
    },
  });
  
  export default examApiSlice.reducer;
  
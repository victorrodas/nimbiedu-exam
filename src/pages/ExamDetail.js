import React, { useState, useEffect } from "react";
import { useParams } from "react-router";
import axios from "axios";

const ExamDetail = (props) => {
  const { id } = useParams();
  const [detail, setDetail] = useState({});
  const [status, setStatus] = useState(true);

  const getDetail = async () => {
    const url = "";
    setStatus(true);
    const response = await axios
      .get(`https://jsonplaceholder.typicode.com/posts/${id}`)
      .then((response) => response.data)
      .catch((error) => error);
    console.log(response);
    setDetail(response);
    setStatus(false);
  };

  useEffect(() => {
    getDetail();
  }, []);

  return <div>
    {status === true && <div>Cargando.....</div>}
      {status === false && (
          <div>
              <div>Title: {detail['title']}</div>
              <div>Description: {detail['body']}</div>
          </div>
      )}
  </div>;
};

export default ExamDetail;

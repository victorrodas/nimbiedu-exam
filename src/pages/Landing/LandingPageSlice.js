import { createSlice } from '@reduxjs/toolkit'

export const landingMusicSlice = createSlice({
    name: 'landingMusicSlice',
    initialState: {
      isPlaying: false
    },
    reducers: {
      play: state => {
        state.isPlaying = true;
      },
      pause: state => {
        state.isPlaying = false;
      }
    }
  });
  
export const { play, pause } = landingMusicSlice.actions
export default landingMusicSlice.reducer
  
import React, { useEffect } from "react";
import { useSelector, useDispatch } from "react-redux";
import { fetchPokemonList } from "./PokeSlice";

const PokePage = (props) => {
  const listPokemon = useSelector(
    (state) => state.landingPokemonsSlice.pokemons
  );
  const status = useSelector((state) => state.landingPokemonsSlice.status);
  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(fetchPokemonList());
  }, [dispatch]);

  return (
    <div>
      <button
        onClick={() => {
          dispatch(fetchPokemonList());
        }}
      >
        cargar de nuevo
      </button>
      {status === "loading" && <div>loading</div>}
      {status !== "loading" && (
        <div>
          {listPokemon.map((item) => {
            return <div>{item["name"]}</div>;
          })}
        </div>
      )}
    </div>
  );
};

export default PokePage;

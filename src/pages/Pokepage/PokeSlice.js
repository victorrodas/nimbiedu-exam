import { createSlice, createAsyncThunk } from "@reduxjs/toolkit";
import axios from "axios";

export const fetchPokemonList = createAsyncThunk(
  "team/fetchPokemonList",
  async (thunkAPI) => {
    await new Promise(function(resolve){
        setTimeout(resolve,5000);
    });
    const response = await axios
      .get(`https://pokeapi.co/api/v2/pokemon?offset=300&limit=100`)
      .then((response) => response.data)
      .catch((error) => error);

    console.log(response);

    return response;
  }
);

export const landingPokemonsSlice = createSlice({
  name: "landingPokemonsSlice",
  initialState: {
    pokemons: [],
    status: "loading",
  },
  reducers: {},
  extraReducers: {
    [fetchPokemonList.pending.type]: (state, action) => {
        state.pokemons = [];
        state.status= "loading";
    },
    [fetchPokemonList.fulfilled.type]: (state, action) => {
        state.pokemons = action.payload.results;
        state.status= "idle";
    },
    [fetchPokemonList.rejected.type]: (state, action) => {
      state.pokemons = []
      state.status= "idle";
    },
  },
});

export default landingPokemonsSlice.reducer;

import { configureStore } from "@reduxjs/toolkit";
import landingMusicSlice from "../pages/Landing/LandingPageSlice";
import examApiSlice from "../pages/ExamApi/ExamApiSlice";
export const store = configureStore({
  reducer: {
    landingMusicSlice,
    examApiSlice,
  },
});

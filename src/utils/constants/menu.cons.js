export const menuNavbar = [
    {
        'url':'/',
        'title':'Home'
    },
    {
        'url':'/ExamApi',
        'title':'Exam Api'
    },
    {
        'url':'/about',
        'title':'About'
    },
    {
        'url':'/contact',
        'title':'Contact'
    },
]